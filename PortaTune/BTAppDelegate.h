//
//  BTAppDelegate.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTAppDelegate : UIResponder <UIApplicationDelegate>
- (void) startRemote;
@property (strong, nonatomic) UIWindow *window;

@end
