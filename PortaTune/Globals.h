//
//  Globals.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 05/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#ifndef PortaTune_Globals_h
#define PortaTune_Globals_h
#import "PTNowPlayingUnderlayViewController.h"

static PTNowPlayingUnderlayViewController *mainUnderlay;

#endif
