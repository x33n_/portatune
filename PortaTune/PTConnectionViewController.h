//
//  PTConnectionViewController.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTServerAPIClient.h"
#import "JWFolders.h"
@interface PTConnectionViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *urlfield;
@property (weak, nonatomic) IBOutlet UITextField *loginfield;
@property (weak, nonatomic) IBOutlet UITextField *passfield;
@property (weak, nonatomic) IBOutlet UISwitch *enableCache;
@property (weak, nonatomic) IBOutlet UISwitch *flacOn3G;
@property (weak, nonatomic) IBOutlet UITextField *localURL;
@property (weak, nonatomic) IBOutlet UITextField *localSSID;
@property (weak, nonatomic) IBOutlet UISwitch *localEnabled;
- (IBAction)goAndConnect:(id)sender;
- (IBAction)didEndOnExit:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *innerDir;
@property (weak, nonatomic) IBOutlet UIButton *lam;
- (IBAction)lamFolderOpener:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *configContainer;

@end
