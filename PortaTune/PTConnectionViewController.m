//
//  PTConnectionViewController.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTConnectionViewController.h"
#import "PTUserConnection.h"
#import "PDKeychainBindings.h"
#import "MBProgressHUD.h"
@interface PTConnectionViewController ()
{
    UIColor*sas;
}
@end

@implementation PTConnectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void) viewWillAppear:(BOOL)animated {
    self.urlfield.text =  [[NSUserDefaults standardUserDefaults]objectForKey:@"access-url"];
    self.loginfield.text = [[PDKeychainBindings sharedKeychainBindings] objectForKey:@"login"];
    self.passfield.text = [[PDKeychainBindings sharedKeychainBindings] objectForKey:@"password"];
    self.localSSID.text = [PTServerAPIClient currentWifiSSID];
    sas = self.innerDir.backgroundColor;
    [self.view setBackgroundColor:[UIColor blackColor]];
    [super viewWillAppear:animated] ;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goAndConnect:(id)sender {
    if (!self.urlfield.text || [self.urlfield.text isEqualToString:@""] ||
        !self.loginfield.text || [self.loginfield.text isEqualToString:@""] ||
        !self.passfield.text || [self.passfield.text isEqualToString:@""] ||
        (self.localEnabled.on && (!self.localSSID.text || [self.localSSID.text isEqualToString:@""] || !self.localURL.text || [self.localURL.text isEqualToString:@""]))) {
        [[[UIAlertView alloc]initWithTitle:@"Fields not filled in" message:@"Please fill in all the fields" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        return;
    }
    [[NSUserDefaults standardUserDefaults]setObject: self.urlfield.text forKey:@"access-url"];
    [[NSUserDefaults standardUserDefaults]setBool:self.localEnabled.on forKey:@"local-enabled"];
    [[NSUserDefaults standardUserDefaults]setObject:self.localSSID.text forKey:@"local-network"];
    [[NSUserDefaults standardUserDefaults]setObject:self.localURL.text forKey:@"local-access"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    [  ( (MBProgressHUD*)[MBProgressHUD HUDForView:self.view]) setDimBackground:true];
    [[PTUserConnection sharedUserConnection] loginWithUsername:self.loginfield.text password:self.passfield.text errorCallback:^(NSString *errorCode) {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:[@"Login failed due to " stringByAppendingString:errorCode] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        return;
    } successCallback:^{
        [[PDKeychainBindings sharedKeychainBindings]setObject:self.loginfield.text forKey:@"login"];
        [[PDKeychainBindings sharedKeychainBindings]setObject:self.passfield.text forKey:@"password"];
        [self dismissModalViewControllerAnimated:true];
    }];
       
}

- (IBAction)didEndOnExit:(id)sender {
}
- (IBAction)lamFolderOpener:(id)sender {
    JWFolders *folder = [JWFolders folder];
    folder.contentView = self.innerDir;
    folder.containerView = self.view;
    folder.position = CGPointMake(self.lam.center.x, self.lam.frame.origin.y + self.configContainer.frame.origin.y );
    folder.direction = JWFoldersOpenDirectionUp;
    folder.contentBackgroundColor = sas;
    folder.shadowsEnabled = YES;
    folder.showsNotch = YES;
    [folder open];
}
@end
