//
//  PTCurrentPlayingListViewController.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 05/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTPlaylistViewController.h"
#import "PTPlayer.h"
@interface PTCurrentPlayingListViewController : PTPlaylistViewController
- (void) updateWithScrolling;
@end
