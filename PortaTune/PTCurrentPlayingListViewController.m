//
//  PTCurrentPlayingListViewController.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 05/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTCurrentPlayingListViewController.h"

@interface PTCurrentPlayingListViewController ()

@end

@implementation PTCurrentPlayingListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.reorderingEnabled = true;
	// Do any additional setup after loading the view.
}
- (void) viewWillAppear:(BOOL)animated {
    //[self.tableView setEditing:true];
    [super viewWillAppear:animated];
}
- (PTPlaylist*)playlist {
    return nil;
}
- (NSArray*)content {
    return [[PTPlayer sharedInstance]curPlaylist];
}
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"FIXME");
    [[PTPlayer sharedInstance] skipRightTo:indexPath.row];
    
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}
- (void) updateWithScrolling {
    [self.tableView reloadData];
    if ([[self content]count] == 0 || [[self content]indexOfObjectIdenticalTo:[[PTPlayer sharedInstance]curTrack]] >= [[self content]count] ) {
        return;
    }
    NSIndexPath*ip  = [NSIndexPath indexPathForRow:[[self content]indexOfObjectIdenticalTo:[[PTPlayer sharedInstance]curTrack]] inSection:0];
    [self.tableView scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:true];
}
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return true;
}
- (void) tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    [[PTPlayer sharedInstance]moveTrackInCurlist:sourceIndexPath.row toPos:destinationIndexPath.row];
}
- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[PTPlayer sharedInstance]dequeueTrackInCurlist:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    }
}
- (bool) hasSearch { return false; }
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
