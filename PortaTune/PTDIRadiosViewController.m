//
//  PTDIRadiosViewController.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 06/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTDIRadiosViewController.h"
#import "PTUserConnection.h"
#import "PTDigitallyImportedTuner.h"
#import "MBProgressHUD.h"
#import "PTPlayer.h"
@implementation PTDIRadiosViewController
- (PTPlaylist*)playlist {
    return dis;
}

- (NSArray*)content {
    return dis.contents;
}
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];   self.navigationItem.title = @"Digitally Imported";
}
bool oneTimeRefresh;
- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!oneTimeRefresh) {
        oneTimeRefresh = true;
        [self refresh];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh) name:@"PortaTuneDidLogin" object:nil];
    }
}
- (void) refresh {
    [self.refreshControl beginRefreshing];
    [PTDigitallyImportedTuner getStationListForCallback:^(PTPlaylist *ret) {
        dis = ret;[self.refreshControl endRefreshing];
        [self.tableView reloadData];
    }];
}
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PTRadioStation*st = dis.contents[indexPath.row];
    if (!st.streamUrl) {
        [MBProgressHUD showHUDAddedTo:self.view animated:true];
        [PTDigitallyImportedTuner resolveStationURLForStation:st callback:^(PTRadioStation *resolvedStation) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:true];
            [[PTPlayer sharedInstance]playRadio:resolvedStation  ];
        }];
    } else
        [[PTPlayer sharedInstance]playRadio:st];
   
    
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    [tableView reloadData];
    UITableViewCell*cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView*i = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"nowp"]];
    [i setFrame:CGRectMake(0, 0, 30, 30)];
    [cell setAccessoryView:i];
    
}
@end
