//
//  PTFileBrowserViewController.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 06/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VDLPlaybackViewController.h"
@interface PTFileBrowserViewController : UITableViewController <VLCMediaPlayerDelegate>
- (PTFileBrowserViewController*) initForFolder:(NSURL*)Folder ;
@end
