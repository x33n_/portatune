//
//  PTFileBrowserViewController.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 06/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTFileBrowserViewController.h"
#import "PTEmptyViewController.h"
@interface PTFileBrowserViewController ()
{
    NSURL*path;
    NSArray* files;
    VLCMediaPlayer*p ;
    PTEmptyViewController*e;
    float oldPriority;
}
@end

@implementation PTFileBrowserViewController
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (PTFileBrowserViewController*) initForFolder:(NSURL*)Folder {
    self = [self init];
    if (self) {
        path = Folder;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    if(!path) path = [self applicationDocumentsDirectory];
    files = [[NSFileManager defaultManager]contentsOfDirectoryAtURL:path includingPropertiesForKeys:Nil options:0 error:nil];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return (files ? files.count : 0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [(NSURL*)files[indexPath.row] lastPathComponent];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier  ];
        [cell.textLabel setText:CellIdentifier];
        BOOL isDir;
        if ([[NSFileManager defaultManager]fileExistsAtPath:[path.path stringByAppendingPathComponent:CellIdentifier] isDirectory:&isDir] && isDir) {
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        }
    }
    // Configure the cell...
    
    return cell;
}
- (void) mediaPlayerStateChanged:(NSNotification *)aNotification {

    if (p.state == VLCMediaPlayerStateStopped) {
        NSLog(@"Restore priority from %f", [[NSThread mainThread]threadPriority]);
        [e dismissModalViewControllerAnimated:true];
        [[NSThread mainThread]setThreadPriority:oldPriority];
    }
}
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    BOOL isDir;
    if ([[NSFileManager defaultManager]fileExistsAtPath:[path.path stringByAppendingPathComponent:[tableView cellForRowAtIndexPath:indexPath].textLabel.text] isDirectory:&isDir] && isDir) {
        PTFileBrowserViewController* f = [[PTFileBrowserViewController alloc]initForFolder:[NSURL URLWithString:[[path path]stringByAppendingPathComponent:[tableView cellForRowAtIndexPath:indexPath].textLabel.text]]];
        [self.navigationController pushViewController:f animated:true];
    } else if ( [[NSFileManager defaultManager]fileExistsAtPath:[path.path stringByAppendingPathComponent:[tableView cellForRowAtIndexPath:indexPath].textLabel.text] isDirectory:&isDir] && !isDir) {
        e = [PTEmptyViewController new];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:e animated:false completion:nil];
        oldPriority = [[NSThread mainThread]threadPriority];
        NSLog(@"Old priority was %f",oldPriority);
        [[NSThread mainThread]setThreadPriority:0];
        NSLog(@"New one %f", [[NSThread mainThread]threadPriority]);
        NSLog(@"Gonna play %@",[[path path]stringByAppendingPathComponent:[tableView cellForRowAtIndexPath:indexPath].textLabel.text]);
        if(!p)p = [[VLCMediaPlayer alloc]init];
        VLCMedia* file = [[VLCMedia alloc]initWithPath:[[path path]stringByAppendingPathComponent:[tableView cellForRowAtIndexPath:indexPath].textLabel.text]];
        [p setDelegate:self];
        [p setDrawable:e.view];
        [p setAdjustFilterEnabled:false];
        [p setScaleFactor:0];
        [p setMedia:file];
        [file parse];
        [p play];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
