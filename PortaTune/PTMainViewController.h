//
//  PTMainViewController.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PTMainViewController : UITabBarController<UITabBarControllerDelegate>
- (void) nowPlaying;
@property (nonatomic) UIViewController *underlay;
@end
