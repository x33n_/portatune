//
//  PTMainViewController.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTMainViewController.h"
#import "PDKeychainBindings.h"
#import "PTConnectionViewController.h"
#import "PTUserConnection.h"
#import "ALAlertBanner.h"
#import <AVFoundation/AVFoundation.h>
#import "MBProgressHUD.h"
#import "MobileVLCKit/MobileVLCKit.h"
#import "PTNowPlayingUnderlayViewController.h"
@interface PTMainViewController ()
{
   
}
@end

@implementation PTMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(_showLocallyWorkingPopup) name:@"PortaTuneDidLogin" object:nil];
	// Do any additional setup after loading the view.
    [[UINavigationBar appearance]setBarStyle:UIBarStyleBlack];
    [self setDelegate:self];
    [self orderTabBar:self];
}

- (void)orderTabBar:(UITabBarController *)tabBarController {
    NSArray *tabsOrderArray = [NSUserDefaults.standardUserDefaults arrayForKey:@"tabBarTabsOrder"];
    if (tabsOrderArray) {
        NSMutableArray *orderedViewControllers = [[NSMutableArray alloc] initWithCapacity:tabBarController.viewControllers.count];
        for (NSNumber *tabOrder in tabsOrderArray) {
            [orderedViewControllers addObject:[self viewControllerForTabBar:tabBarController TabBarItemTag:tabOrder.integerValue]];
        }
        tabBarController.viewControllers = orderedViewControllers;
    }
}

- (UIViewController *)viewControllerForTabBar:(UITabBarController *)tabBarController TabBarItemTag:(NSInteger)tag {
    for (UIViewController *viewController in tabBarController.viewControllers)
        if (viewController.tabBarItem.tag == tag)
            return viewController;
    return nil;
}

- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed {
    
    NSMutableArray *tabsOrderArray = [[NSMutableArray alloc] initWithCapacity:tabBarController.viewControllers.count];
    for (UIViewController *viewController in viewControllers) {
        [tabsOrderArray addObject:[NSNumber numberWithInt:viewController.tabBarItem.tag]];
    }
    
    [NSUserDefaults.standardUserDefaults setObject:[NSArray arrayWithArray:tabsOrderArray] forKey:@"tabBarTabsOrder"];
    [NSUserDefaults.standardUserDefaults synchronize];
}

- (void) _showLocallyWorkingPopup {
    
    if (![PTServerAPIClient isWorkingLocally]) return;
    ALAlertBanner*b = [ALAlertBanner alertBannerForView:self.view style:ALAlertBannerStyleNotify position:ALAlertBannerPositionTop title:@"Local Network Access Mode" subtitle:@"No musical traffic is going outside your WiFi network" tappedBlock:nil];
    b.showAnimationDuration = 0.2f; b.hideAnimationDuration = 0.2f;
    b.secondsToShow = 2.4f;

    [b show];
}
- (void) nowPlaying {
    [(PTNowPlayingUnderlayViewController*)self.underlay showTopView];
}
- (unsigned long long int)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    unsigned long long int fileSize = 0;
    
    for(NSString*fileName in filesArray) {
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:nil];
        fileSize += [fileDictionary fileSize];
    }
    return fileSize;
}
static bool shouldRunOnce;
- (void) viewDidAppear:(BOOL)animated {
  
    NSLog(@"мы вращаемся");
    if (!shouldRunOnce) {
        shouldRunOnce = true;
        if ([[NSUserDefaults standardUserDefaults]boolForKey:@"resetcred"]) {
            [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"resetcred"];
            [[NSUserDefaults standardUserDefaults]setObject: @"" forKey:@"access-url"];
            [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"local-enabled"];
            [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"local-network"];
            [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"local-access"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[PDKeychainBindings sharedKeychainBindings]setObject:@"" forKey:@"login"];
            [[PDKeychainBindings sharedKeychainBindings]setObject:@"" forKey:@"password"];
            [[PDKeychainBindings sharedKeychainBindings]setObject:@"" forKey:@"vktoken"];
            
            [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"cache-wipe"];
        }
        NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *cacheDir    = [myPathList  objectAtIndex:0];
        if ([[NSUserDefaults standardUserDefaults]boolForKey:@"cache-wipe"]) {
            ALAlertBanner*b = [ALAlertBanner alertBannerForView:self.view style:ALAlertBannerStyleWarning position:ALAlertBannerPositionTop title:@"Caches Wiped" subtitle:@"All newly played songs will be re-cached when you enter Local Access Mode. Data fees may apply!" tappedBlock:nil];
            b.showAnimationDuration = 0.2f; b.hideAnimationDuration = 0.2f;
            b.secondsToShow = 2.4f;[b show];
            [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"cache-wipe"];
           
            for (NSString*paths in [[NSFileManager defaultManager]contentsOfDirectoryAtPath:cacheDir error:nil]) {
                [[NSFileManager defaultManager]removeItemAtPath:paths error:nil];
            }
        }
        [[NSUserDefaults standardUserDefaults]setObject:[NSByteCountFormatter stringFromByteCount:[self folderSize:cacheDir] countStyle:NSByteCountFormatterCountStyleFile] forKey:@"cache-size"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        if ([ [[NSUserDefaults standardUserDefaults]objectForKey:@"access-url"] isEqualToString:@""] || [[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"login"]isEqualToString:@""] || [[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"password"]isEqualToString:@""] ||  [[NSUserDefaults standardUserDefaults]objectForKey:@"access-url"] == nil || [[PDKeychainBindings sharedKeychainBindings] objectForKey:@"login"] == nil || [[PDKeychainBindings sharedKeychainBindings] objectForKey:@"password"] == nil) {
            NSLog(@"Ивновьонвы");
            [self presentModalViewController:[PTConnectionViewController new] animated:true];
        } else {
            [MBProgressHUD showHUDAddedTo:self.view animated:true];
            [[PTUserConnection sharedUserConnection] loginWithUsername:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"login"] password:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"password"] errorCallback:^(NSString *errorCode) {
                [MBProgressHUD hideAllHUDsForView:self.view animated:true];
                [self presentModalViewController:[PTConnectionViewController new] animated:true];
            } successCallback:^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:true];
            }];
        }
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
