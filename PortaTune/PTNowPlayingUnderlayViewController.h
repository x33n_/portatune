//
//  PTNowPlayingUnderlayViewController.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 05/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTPlayer.h"
#import "PTMainViewController.h"
#import "PTCurrentPlayingListViewController.h"
#import "MarqueeLabel.h"
@interface PTNowPlayingUnderlayViewController : UIViewController <PTPlayerGUIController>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *moarView;
- (IBAction)showMeMoar:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *moarButton;
@property (weak, nonatomic) IBOutlet UIButton *next;
@property (weak, nonatomic) IBOutlet UIButton *prev;
@property (weak, nonatomic) IBOutlet UIButton *play;
@property (weak, nonatomic) IBOutlet UIButton *paus;
@property (weak, nonatomic) IBOutlet UIButton *shuffle;
@property (weak, nonatomic) IBOutlet UISlider *seeker;
@property (weak, nonatomic) IBOutlet UILabel *timeelapse;
@property (weak, nonatomic) IBOutlet UILabel *timeremain;
@property (weak, nonatomic) IBOutlet UILabel *tracknumber;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet MarqueeLabel *titleview;
@property (weak, nonatomic) IBOutlet UIView *playlistHolder;
- (IBAction)seekerTouchup:(id)sender;
- (void) showTopView;
@end
