//
//  PTNowPlayingUnderlayViewController.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 05/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTNowPlayingUnderlayViewController.h"
#import "Globals.h"
#import "PTServerAPIClient.h"
#import "ALAlertBanner.h"
@interface PTNowPlayingUnderlayViewController ()
{
    PTMainViewController* main; UITapGestureRecognizer* grec;
    BOOL isShowingMoarView;
    bool isShowingTopView;
    BOOL isShowingTrackTicker;
    NSTimeInterval totalDur;
    PTCurrentPlayingListViewController* cur;
    bool wasPlay;
}
@end

@implementation PTNowPlayingUnderlayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void) showTrackTicker {
    if (isShowingMoarView || isShowingTrackTicker || isShowingTopView) return;
    [UIView beginAnimations:@"showTopView" context:nil];
    
    [main.view setFrame:CGRectOffset(main.view.frame, 0, 40)];
    [self.topView setFrame:CGRectOffset(self.topView.frame, 0, -5)];
    [self.topView setTransform:CGAffineTransformScale(CGAffineTransformIdentity, 1,1)];
    [self.topView setAlpha:1];
    
    [UIView commitAnimations];
    isShowingTrackTicker = true;
    [self performSelector:@selector(hideTrackTicker) withObject:nil afterDelay:5];
}
- (void) hideTrackTicker {
        if (isShowingMoarView || !isShowingTrackTicker || isShowingTopView) return;
    [UIView beginAnimations:@"showTopView" context:nil];
    
    [main.view setFrame:CGRectOffset(main.view.frame, 0, -40)];
    [self.topView setFrame:CGRectOffset(self.topView.frame, 0, 5)];
    [self.topView setTransform:CGAffineTransformScale(CGAffineTransformIdentity, 0.9,0.9)];
    [self.topView setAlpha:1];
    
    [UIView commitAnimations];
    isShowingTrackTicker = false;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    main = [self.storyboard instantiateViewControllerWithIdentifier:@"MainTabs"];
    [main.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:main.view];
    [main setUnderlay:self];
 /*   [main.view.layer setShadowColor:[UIColor blackColor].CGColor];
    [main.view.layer setShadowOpacity:0.4f];
    [main.view.layer setShadowRadius:0.5f];
    [self.topView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.topView.layer setShadowOpacity:0.4f];
    [self.topView.layer setShadowRadius:0.5f]; */
    [self.topView setFrame:CGRectOffset(self.topView.frame, 0, 5)];
    [self.topView setTransform:CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9)];
    [self.topView setAlpha:0.7];
    [self.moarView setTransform:CGAffineTransformScale(CGAffineTransformIdentity, 0.6, 0.6)];
    [self.moarView setAlpha:0.4];
    [self.view sendSubviewToBack:self.moarView];
    [self.titleview setMarqueeType:MLContinuous];
    [self.titleview setAnimationDelay:0.2f];
    [self.titleview setBackgroundColor:[UIColor clearColor]];
    [self.titleview setFadeLength:2];
    [self.titleview setTextColor:[UIColor whiteColor]];
    [[PTPlayer sharedInstance]setGUI:self];
    
    cur = [PTCurrentPlayingListViewController new];
    [cur.view setFrame:CGRectMake(0, 0, self.playlistHolder.frame.size.width, self.playlistHolder.frame.size.height)];
    [self.playlistHolder addSubview:cur.view];
    mainUnderlay = self;
}
- (IBAction)seekerTouchdown:(id)sender {
    wasPlay = [[PTPlayer sharedInstance]isPlaying];
    [[PTPlayer sharedInstance]pause];
}



- (IBAction)showMeMoar:(id)sender {
    (isShowingMoarView ? [self hideMoarView] : [self showMoarView]);
    [cur updateWithScrolling];
}
- (IBAction)pauseTap:(id)sender {
    [[PTPlayer sharedInstance]pause];
    [self.play setHidden:false];
    [self.paus setHidden:true];
}
- (IBAction)playTap:(id)sender {
    [[PTPlayer sharedInstance]play];
    [self.play setHidden:true];
    [self.paus setHidden:false];
}
- (IBAction)nextTap:(id)sender {
    [[PTPlayer sharedInstance]next];
    [cur updateWithScrolling];
}
- (IBAction)prevTap:(id)sender {
    [[PTPlayer sharedInstance]previous];
    [cur updateWithScrolling];
    [cur.tableView setNeedsLayout];
}
- (IBAction)shuffleTap:(UIButton*)sender {
        [sender setSelected:!sender.selected];
        [[PTPlayer sharedInstance]setShuffling:sender.selected];
}
- (IBAction)seeking:(id)sender {
    [[PTPlayer sharedInstance]seekTo:self.seeker.value];
}

-(void) updateSeekerToPosition:(float)position {
    [self.seeker setValue:position animated:true];
   // [self.timeelapse setText:[self formattedStringForDuration:(totalDur * position)]];
}
- (NSString*)formattedStringForDuration:(NSTimeInterval)duration
{
    NSInteger minutes = floor(duration/60);
    NSInteger seconds = round(duration - minutes * 60);
    return [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
}

- (void) updateStateToPlaying:(BOOL)isPlaying {
    [self.play setHidden:isPlaying];
    [self.paus setHidden:!isPlaying];
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"didShowCaching"]&&[PTServerAPIClient isWorkingLocally]) {
        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"didShowCaching"];
        ALAlertBanner*b = [ALAlertBanner alertBannerForView:self.view style:ALAlertBannerStyleNotify position:ALAlertBannerPositionTop title:@"Want to spend less traffic on the go?" subtitle:@"Enable caching options in PortaTune panel in your phone's Settings" tappedBlock:nil];
        b.showAnimationDuration = 0.2f; b.hideAnimationDuration = 0.2f;
        b.secondsToShow = 2.4f;
        [b show];
    }
}
- (void) updateTitle: (NSString*)title number:(NSString*)number totalTime:(NSNumber*)totalTime {
    [self.titleview setText:title];
    [self.tracknumber setText:number];
    [self showTrackTicker];
    [cur updateWithScrolling];
   // NSLog(@"%@", totalTime);
    
   // totalDur = totalTime;
   // [self.timeremain setText:[self formattedStringForDuration:totalDur]];
}

- (void) updateToRadio:(BOOL)isRadio {
        if(isShowingMoarView && isRadio)  [self hideMoarView];
        [self.moarButton setEnabled:!isRadio];
        [self.prev setEnabled:!isRadio];
        [self.next setEnabled:!isRadio];
    
}

/*
- (void) updateBuffering:(BOOL)isBuffering {
    [UIView beginAnimations:@"a" context:nil];
    if (!isBuffering) {
        [self.titleview setFrame:CGRectMake(20, 10, 280, 26)];
        [self.spinner setAlpha:0];
    } else {
        [self.titleview setFrame:CGRectMake(20, 10, 252, 26)];
        [self.spinner setAlpha:1];
    }
         [UIView commitAnimations];
}
 */
- (IBAction)seekerTouchup:(id)sender {
    if(wasPlay) [[PTPlayer sharedInstance]play];
}

- (void) showTopView {
   // main.view.userInteractionEnabled = false;
    if(isShowingTopView) return;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideTrackTicker) object:nil];
    [UIView beginAnimations:@"showTopView" context:nil];
    
    [main.view setFrame:CGRectOffset(main.view.frame, 0, (isShowingTrackTicker ? 60 : 100))];
    if(!isShowingTrackTicker) {
        [self.topView setFrame:CGRectOffset(self.topView.frame, 0, -5)];
    }
    [self.topView setTransform:CGAffineTransformScale(CGAffineTransformIdentity, 1,1)];
    [self.topView setAlpha:1];
    
    [UIView commitAnimations];
    
    if(!grec) grec = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideTopView)];
    [grec setNumberOfTapsRequired:1]; [grec setNumberOfTouchesRequired:1];
    [main.view addGestureRecognizer:grec];
    isShowingTopView = true;
    isShowingTrackTicker = false;
}
- (void) showMoarView {
    if (isShowingMoarView || !isShowingTopView) return;
    
    [UIView beginAnimations:@"showMoarView" context:nil];
    
    [main.view setFrame:CGRectOffset(main.view.frame, 0, 300)];
    [self.moarView setTransform:CGAffineTransformScale(CGAffineTransformIdentity, 1, 1)];
    [self.moarView setAlpha:1];
    
    [UIView commitAnimations];
    
    isShowingMoarView = true;
}
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if ( event.subtype == UIEventSubtypeMotionShake )
    {
        [self showTopView];
    }
    
    if ( [super respondsToSelector:@selector(motionEnded:withEvent:)] )
        [super motionEnded:motion withEvent:event];
}
- (void) hideMoarView {
    if (!isShowingMoarView) return;
    
    [UIView beginAnimations:@"hideMoarView" context:nil];
    
    [main.view setFrame:CGRectOffset(main.view.frame, 0, -300)];
    [self.moarView setTransform:CGAffineTransformScale(CGAffineTransformIdentity, 0.6, 0.6)];
    [self.moarView setAlpha:0.4];
    
    [UIView commitAnimations];
    
    isShowingMoarView = false;
}
- (void) hideTopView {
    [main.view removeGestureRecognizer:grec];
    
    [UIView beginAnimations:@"hideTopView" context:nil];
    
    [main.view setFrame:CGRectOffset(main.view.frame, 0, (isShowingMoarView ? -400 : -100))];
    [self.topView setFrame:CGRectOffset(self.topView.frame, 0, 5)];
    [self.topView setTransform:CGAffineTransformScale(CGAffineTransformIdentity, 0.9,0.9)];
    [self.topView setAlpha:0.7];
    [self.moarView setTransform:CGAffineTransformScale(CGAffineTransformIdentity, 0.6, 0.6)];
    [self.moarView setAlpha:0.4];

    
    [UIView commitAnimations];
    main.view.userInteractionEnabled = true;
    isShowingMoarView = false;
    isShowingTopView = false;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
