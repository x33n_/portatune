//
//  PTPlayer.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PTPlaylist.h"
#import "PTTrack.h"
#import "MobileVLCKit.h"
#import "PTVKComm.h"
#import "PTRadioStation.h"
@protocol PTPlayerGUIController;
@interface PTPlayer : UIResponder <VLCMediaPlayerDelegate,VLCMediaDelegate,VLCMediaListDelegate>
+ (PTPlayer*) sharedInstance;

- (void) playList:(PTPlaylist*)list startingFrom:(NSInteger)trackIdx;
- (void) playRadio: (PTRadioStation*)radio;
- (void) playTrack:(PTTrack*)track inList:(PTPlaylist*)list;
- (void) _playTrackViaVLC:(PTTrack*)track;


- (NSArray*)curPlaylist;
- (void) moveTrackInCurlist:(NSInteger)track toPos:(NSInteger)pos;
- (void) dequeueTrackInCurlist:(NSInteger)track;


- (PTTrack*)curTrack;

- (BOOL)isPlaying;
- (BOOL)isRadio;
- (void) play;
- (void) pause;
- (void) seekTo: (float)to;
- (void) next;
- (void) previous;
- (void) setShuffling: (BOOL)shuffle;
- (void) skipRightTo: (NSInteger)to;

@property id<PTPlayerGUIController> GUI;
@end

@protocol PTPlayerGUIController <NSObject>

- (void) updateSeekerToPosition: (float)position;
- (void) updateStateToPlaying:(BOOL)isPlaying;
- (void) updateBuffering: (BOOL)isBuffering;
- (void) updateTitle: (NSString*)title number:(NSString*)number totalTime:(NSNumber*)totalTime;
- (void) updateToRadio:(BOOL)isRadio;

@end