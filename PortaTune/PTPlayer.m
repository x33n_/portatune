//
//  PTPlayer.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTPlayer.h"
#import "PTServerAPIClient.h"
#import "PTSoundCloud.h"
#import "BTAppDelegate.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@interface NSString (isin)
- (BOOL)isNotAmongThese:(NSArray*)which;
@end
@implementation NSString (isin)

-(BOOL)isNotAmongThese:(NSArray *)which {
    return ![which containsObject:self];
}
@end

@interface NSMutableArray (Shuffling)
- (void)shuffle;
@end
@implementation NSMutableArray (Shuffling)
- (void)shuffle
{
    NSUInteger count = [self count];
    for (NSUInteger i = 0; i < count; ++i) {
        // Select a random element between i and end of array to swap with.
        NSInteger nElements = count - i;
        NSInteger n = arc4random_uniform(nElements) + i;
        [self exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
}
@end

@interface NSMutableArray (MoveArray)

- (void)moveObjectFromIndex:(NSUInteger)from toIndex:(NSUInteger)to;

@end

@implementation NSMutableArray (MoveArray)

- (void)moveObjectFromIndex:(NSUInteger)from toIndex:(NSUInteger)to
{
    if (to != from) {
        id obj = [self objectAtIndex:from];
        [self removeObjectAtIndex:from];
        if (to >= [self count]) {
            [self addObject:obj];
        } else {
            [self insertObject:obj atIndex:to];
        }
    }
}
@end
@interface PTPlayer ()
{
    PTPlaylist * currentPlaylist;
    NSMutableArray* playlistContent;
    NSMutableArray* shuffledPlaylistContent;
    PTTrack* currentTrack;
    VLCMediaPlayer*vlc;
    bool shufflin;
}
@end

@implementation PTPlayer

+ (PTPlayer *)sharedInstance
{
	static PTPlayer *sharedInstance = nil;
	if (sharedInstance == nil)
	{
		sharedInstance = [[self alloc] init];
	}
	return sharedInstance;
}
- (void) media:(VLCMedia *)aMedia metaValueChangedFrom:(id)oldValue forKey:(NSString *)key {
    if ([key isEqualToString:VLCMetaInformationAlbum]||[key isEqualToString:VLCMetaInformationArtist]||[key isEqualToString:VLCMetaInformationTitle]) {
        currentTrack.album = [aMedia metaDictionary][VLCMetaInformationAlbum];
        currentTrack.artist = [aMedia metaDictionary][VLCMetaInformationArtist];
        currentTrack.name = [aMedia metaDictionary][VLCMetaInformationTitle];
          NSLog(@"Art %@",aMedia.metaDictionary[VLCMetaInformationArtworkURL]);
        [[MPNowPlayingInfoCenter defaultCenter]setNowPlayingInfo:@{MPMediaItemPropertyArtist: currentTrack.artist, MPMediaItemPropertyTitle: currentTrack.name, MPMediaItemPropertyAlbumTitle: currentTrack.album, MPMediaItemPropertyAlbumTrackCount: [NSNumber numberWithInt:(currentTrack.containingPlaylist ? currentTrack.containingPlaylist.contents.count : 1)], MPMediaItemPropertyAlbumTrackNumber: [NSNumber numberWithInt:(currentTrack.containingPlaylist ? [currentTrack.containingPlaylist.contents indexOfObject:currentTrack] : 1)]}];

    }
    NSLog(@"Meta From:%@ Key:%@", oldValue,key);
}
- (void) mediaDidFinishParsing:(VLCMedia *)aMedia {
    NSLog(@"Finished parsing %@",aMedia.metaDictionary);
    @try {
        currentTrack.album = [aMedia metaDictionary][VLCMetaInformationAlbum];
        currentTrack.artist = [aMedia metaDictionary][VLCMetaInformationArtist];
        currentTrack.name = [aMedia metaDictionary][VLCMetaInformationTitle];
        NSLog(@"Art %@",aMedia.metaDictionary[VLCMetaInformationArtworkURL]);
        
        [[MPNowPlayingInfoCenter defaultCenter]setNowPlayingInfo:@{MPMediaItemPropertyArtist: currentTrack.artist, MPMediaItemPropertyTitle: currentTrack.name, MPMediaItemPropertyAlbumTitle: currentTrack.album, MPMediaItemPropertyAlbumTrackCount: [NSNumber numberWithInt:(currentTrack.containingPlaylist ? currentTrack.containingPlaylist.contents.count : 1)], MPMediaItemPropertyAlbumTrackNumber: [NSNumber numberWithInt:(currentTrack.containingPlaylist ? [currentTrack.containingPlaylist.contents indexOfObject:currentTrack] : 1)]}];
    } @catch (NSException*e) {
        
    }
}


- (void) mediaPlayerStateChanged:(NSNotification *)aNotification {
  [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:(vlc.state ==VLCMediaPlayerStateBuffering)];
    if ([self.GUI respondsToSelector:@selector(updateBuffering:)])
        [self.GUI updateBuffering:(vlc.state ==VLCMediaPlayerStateBuffering)];
    if (vlc.state == VLCMediaPlayerStateStopped) {
        [self next];
    }
    if ([self.GUI respondsToSelector:@selector(updateStateToPlaying:)])
        [self.GUI updateStateToPlaying:(vlc.isPlaying)];
      NSLog(@"Art %@",vlc.media.metaDictionary[VLCMetaInformationArtworkURL]);
    NSLog(@"StN: %@", VLCMediaPlayerStateToString(vlc.state));
}
- (void) mediaPlayerTimeChanged:(NSNotification *)aNotification {
    if ([self.GUI respondsToSelector:@selector(updateSeekerToPosition:)])
        [self.GUI updateSeekerToPosition:vlc.position];
}
- (void) skipRightTo: (NSInteger)to {
    if (currentTrack.isRadioStation) return;
    if (shufflin) {
        currentTrack = shuffledPlaylistContent [to];
    } else {
        currentTrack = playlistContent[to];
    }
    [self _playTrackViaVLC:currentTrack];
}

- (void) _playTrackViaVLC:(PTTrack *)track {
    if (currentTrack) currentTrack.isNowPlaying = false;
    if(!vlc) {
        vlc = [[VLCMediaPlayer alloc]init];
        [vlc setDelegate:self];
    }
    NSURL *reurl = nil;
    currentTrack = track;
    currentTrack.isNowPlaying =true;
    if ([[track.identifier substringToIndex:2] isEqualToString:@"SC"])reurl = [PTSoundCloud getTrackURLForPTTID:track.identifier];
    else if ([[track.identifier substringToIndex:2] isEqualToString:@"VK"]) {
        if (track.extradata) {
            reurl = [NSURL URLWithString:track.extradata]  ;
        }else {
            [PTVKComm resolveTrack:track callback:^(PTTrack *tr) {
                [self _playTrackViaVLC:tr];
            }];
        
            return;
        }
    }
    else reurl = [NSURL URLWithString:[[PTServerAPIClient makeLinkForAudiofile:track.identifier]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    [vlc stop];
    VLCMedia* media = [[VLCMedia alloc]initWithURL:reurl ];
    [media setDelegate:self];
    [vlc setMedia:media];
    [vlc play];
    [media parse];
    if ([self.GUI respondsToSelector:@selector(updateToRadio:)]) {
        [self.GUI updateToRadio:NO];
    }
    [(BTAppDelegate*)[UIApplication sharedApplication].delegate startRemote];
    [[MPNowPlayingInfoCenter defaultCenter]setNowPlayingInfo:@{MPMediaItemPropertyArtist: track.artist, MPMediaItemPropertyTitle: track.name, MPMediaItemPropertyAlbumTitle: track.album, MPMediaItemPropertyAlbumTrackCount: [NSNumber numberWithInt:(track.containingPlaylist ? track.containingPlaylist.contents.count : 1)], MPMediaItemPropertyAlbumTrackNumber: [NSNumber numberWithInt:(track.containingPlaylist ? [track.containingPlaylist.contents indexOfObject:track] : 1)]}];
    
    if ([self.GUI respondsToSelector:@selector(updateTitle:number:totalTime:)])
        [self.GUI updateTitle:[currentTrack userReadableRepresentation] number:[NSString stringWithFormat:@"%i of %i" ,[ (shufflin ? shuffledPlaylistContent : playlistContent)indexOfObjectIdenticalTo:track]+1, [(shufflin ? shuffledPlaylistContent : playlistContent) count]] totalTime:[media.length numberValue]];
      NSLog(@"Art1st %@",media.metaDictionary[VLCMetaInformationArtworkURL]);
}

- (void) playList:(PTPlaylist*)list startingFrom:(NSInteger)trackIdx {
    if (![list isEqual:currentPlaylist]) {
        currentPlaylist = list;
        playlistContent = [currentPlaylist.contents mutableCopy]  ;
    }
    if (shufflin) {
        shuffledPlaylistContent = [currentPlaylist.contents mutableCopy];
        [shuffledPlaylistContent shuffle];
        [shuffledPlaylistContent exchangeObjectAtIndex:[shuffledPlaylistContent indexOfObjectIdenticalTo:playlistContent[trackIdx]] withObjectAtIndex:0];
        currentTrack = shuffledPlaylistContent[0];
    } else {
        currentTrack = playlistContent[trackIdx];
    }
    
    [self _playTrackViaVLC:currentTrack];
}

- (void) moveTrackInCurlist:(NSInteger)track toPos:(NSInteger)pos {
    if (currentTrack.isRadioStation) return;
    [(shufflin ? shuffledPlaylistContent : playlistContent) moveObjectFromIndex:track toIndex:pos];
}
- (void) dequeueTrackInCurlist:(NSInteger)track {
    if (currentTrack.isRadioStation) return;
    [(shufflin ? shuffledPlaylistContent : playlistContent) removeObjectAtIndex:track];
}
- (void) playRadio: (PTRadioStation*)radio {
    if (![radio isRadioStation]) return;
    if (vlc) [vlc stop];
    else {
        vlc = [[VLCMediaPlayer alloc]init];
        [vlc setDelegate:self];
    }
    if(currentTrack) currentTrack.isNowPlaying = false;
    
    currentTrack = radio;
    radio.isNowPlaying = true;
    
    
    playlistContent = [NSMutableArray new];
    [playlistContent addObject:radio];
    shuffledPlaylistContent = [NSMutableArray new];
    VLCMedia* media = [[VLCMedia alloc]initWithURL:[NSURL URLWithString:radio.streamUrl] ];
    [media setDelegate:self];
    [vlc setMedia:media];
    [vlc play];
    [media parse];
    if ([self.GUI respondsToSelector:@selector(updateToRadio:)]) {
        [self.GUI updateToRadio:YES];
    }
    [(BTAppDelegate*)[UIApplication sharedApplication].delegate startRemote];
    [[MPNowPlayingInfoCenter defaultCenter]setNowPlayingInfo:@{ MPMediaItemPropertyTitle: currentTrack.name}];
    if ([self.GUI respondsToSelector:@selector(updateTitle:number:totalTime:)])
        [self.GUI updateTitle:[NSString stringWithFormat:@"Radio: %@",radio.name]   number:@"" totalTime:0];

}

- (void) setShuffling: (BOOL)shuffle {
    if (currentTrack.isRadioStation) return;
    shufflin=shuffle;
    if (shufflin) {
        shuffledPlaylistContent = [currentPlaylist.contents mutableCopy];
        [shuffledPlaylistContent shuffle];
        if(currentTrack)[shuffledPlaylistContent exchangeObjectAtIndex:[shuffledPlaylistContent indexOfObjectIdenticalTo:currentTrack] withObjectAtIndex:0];
        currentTrack = shuffledPlaylistContent[0];
    } else {
        currentTrack = (currentTrack ?  playlistContent[[playlistContent indexOfObjectIdenticalTo:currentTrack]] : 0 );
    }
}
- (void) playTrack:(PTTrack*)track inList:(PTPlaylist*)list {
    if (![list isEqual:currentPlaylist]) {
        NSInteger idx = [list.contents indexOfObjectIdenticalTo:track];
        [self playList:list startingFrom:idx];
    } else {
        NSMutableArray*p  = (shufflin ? shuffledPlaylistContent : playlistContent);
        NSInteger idx= [p indexOfObjectIdenticalTo:track];
        if (idx>0 && idx<p.count-1)
            [self _playTrackViaVLC:p[idx]];
    }
    
}

- (void) next {
    if (currentTrack.isRadioStation) return;
    NSMutableArray*p  = (shufflin ? shuffledPlaylistContent : playlistContent);
    NSInteger idx= [p indexOfObjectIdenticalTo:currentTrack];
    if (idx < p.count-2) {
        [self _playTrackViaVLC:p[idx+1]];
    }
}

-(void) previous {
    if (currentTrack.isRadioStation) return;
    NSMutableArray*p  = (shufflin ? shuffledPlaylistContent : playlistContent);
    NSInteger idx= [p indexOfObjectIdenticalTo:currentTrack];
    if (idx >= 1) {
        [self _playTrackViaVLC:p[idx-1]];
    }
}

- (PTTrack*)curTrack {
    return currentTrack;
}
- (NSArray*)curPlaylist { return (shufflin ? shuffledPlaylistContent : playlistContent) ; }
- (BOOL)isPlaying { return vlc.isPlaying; }
- (BOOL)isRadio { return [currentTrack isRadioStation]; }
- (void) play {
    NSError *setCategoryErr = nil;
    NSError *activationErr  = nil;
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error:&setCategoryErr];
    [[AVAudioSession sharedInstance] setActive:YES error:&activationErr];
    [vlc play];
    [(BTAppDelegate*)[[UIApplication sharedApplication]delegate] startRemote];
    if ([self.GUI respondsToSelector:@selector(updateStateToPlaying:)])
        [self.GUI updateStateToPlaying:false];
   
}
- (void) pause {
    [[AVAudioSession sharedInstance] setActive:NO error:nil];
    [vlc pause];
    if ([self.GUI respondsToSelector:@selector(updateStateToPlaying:)])
        [self.GUI updateStateToPlaying:false];
}


- (void) seekTo: (float)to {
    if (to > 1.0 || to < 0.0 || !vlc)
        return;
    bool wasPlay = [self isPlaying];
    if(!wasPlay) [vlc play];
    [vlc setPosition:to];
    if(!wasPlay) [vlc pause];
}




@end
