//
//  PTPlaylist.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTPlaylist.h"

@implementation PTPlaylist
- (NSString*)description {
    return [NSString stringWithFormat:@"Playlist %i '%@':\n %@", self.identifier, self.name, self.contents];
}
@end
