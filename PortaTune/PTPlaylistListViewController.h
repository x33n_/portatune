//
//  PTPlaylistListViewController.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTMainViewController.h"
@interface PTPlaylistListViewController : UITableViewController

@end
