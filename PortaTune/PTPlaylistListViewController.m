//
//  PTPlaylistListViewController.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTPlaylistListViewController.h"
#import "PTUserConnection.h"
#import "ISRefreshControl.h"
#import "PTPlaylistViewController.h"

@interface PTPlaylistListViewController ()
{
    NSArray* allLists;
}
@end

@implementation PTPlaylistListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.refreshControl = (id)[[UIRefreshControl alloc] init];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh) name:@"PortaTuneDidLogin" object:nil];
    [self.refreshControl addTarget:self
                            action:@selector(refresh)
                  forControlEvents:UIControlEventValueChanged];
    if (self.navigationController && self.tabBarController) {
        if (self.navigationController.navigationBar.gestureRecognizers.count <= 0) {
            UISwipeGestureRecognizer *swipeUpDown = [[UISwipeGestureRecognizer alloc] initWithTarget:(PTMainViewController*)self.tabBarController action:@selector(nowPlaying)];
            [swipeUpDown setDirection:(UISwipeGestureRecognizerDirectionDown )];
            [self.navigationController.navigationBar addGestureRecognizer:swipeUpDown];
        }
        if (!self.navigationItem.rightBarButtonItem) {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Now Playing" style:UIBarButtonItemStyleBordered target:(PTMainViewController*)self.tabBarController action:@selector(nowPlaying)];
        }
    }
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (NSArray*)lists { return allLists; }
- (IBAction)showNowPlaying:(id)sender {
    [(PTMainViewController*)self.tabBarController nowPlaying];
}
- (void) refresh {
    [self.refreshControl beginRefreshing];
     PTUserConnection*u = [PTUserConnection sharedUserConnection];
    if ([u isLoggedIn]) {
        [u getPlaylistsForCallback:^(NSArray *playlists) {
            allLists = [playlists copy];
            [self.tableView reloadData];
            [self.refreshControl endRefreshing];
        }];
    } else {
        
    }
}
bool oneTimeRefresh;
- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!oneTimeRefresh) {
        oneTimeRefresh = true;
        [self refresh];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh) name:@"PortaTuneDidLogin" object:nil];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return ([self lists] ? [self lists].count : 0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PTPlaylist*pls = (PTPlaylist*) [self lists][indexPath.row] ;
    
     NSString *CellIdentifier = [NSString stringWithFormat:@"%i",[pls identifier]];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == Nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell.textLabel setText: [pls name]];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    
    // Configure the cell...
    
    return cell;
}
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.navigationController pushViewController:[[PTPlaylistViewController alloc]initForPlaylist:[self lists][indexPath.row]] animated:true];
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
