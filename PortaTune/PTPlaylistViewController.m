//
//  PTPlaylistViewController.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTPlaylistViewController.h"
#import "PTTrack.h"
#import "PTPlayer.h"
#import "PTUserConnection.h"
#import "MBProgressHUD.h"
#import "PTMainViewController.h"

@interface NSString (JRStringAdditions)

- (BOOL)containsString:(NSString *)string;
- (BOOL)containsString:(NSString *)string
               options:(NSStringCompareOptions)options;

@end

@implementation NSString (JRStringAdditions)

- (BOOL)containsString:(NSString *)string
               options:(NSStringCompareOptions)options {
    NSRange rng = [self rangeOfString:string options:options];
    return rng.location != NSNotFound;
}

- (BOOL)containsString:(NSString *)string {
    return [self containsString:string options:0];
}

@end

@interface PTPlaylistViewController ()
{
    PTPlaylist* curPlaylist;
    NSArray* content;
    NSMutableArray*searchdta;
    UISearchBar *searchBar;
    UISearchDisplayController *searchDisplayController;
}
@end

@implementation PTPlaylistViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (PTPlaylistViewController*)initForPlaylist:(PTPlaylist*)playlist {
    self = [self init];
    if (self) {
        curPlaylist = playlist;
        if (!curPlaylist.contents) {
            if(curPlaylist.isAlbum) {
                [MBProgressHUD showHUDAddedTo:self.view animated:true];
                [[PTUserConnection sharedUserConnection]getAlbum:playlist ForCallback:^(PTPlaylist *ses) {
                    [MBProgressHUD hideAllHUDsForView:self.view animated:true];
                    curPlaylist = ses;content = curPlaylist.contents;
                    [self.tableView reloadData];
                }];
            }
        } else
            content = [curPlaylist contents];
        
    }
    return self;
}
- (PTPlaylist*)playlist {
    return curPlaylist;
}
- (NSArray*)content {
    return curPlaylist.contents;
}
- (bool) hasSearch { return true; }
- (void)viewDidLoad
{
    [super viewDidLoad];
    searchdta = [NSMutableArray new];
    self.reorderingEnabled = false;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    if (self.navigationController && self.tabBarController) {
        if (self.navigationController.navigationBar.gestureRecognizers.count <= 0) {
            UISwipeGestureRecognizer *swipeUpDown = [[UISwipeGestureRecognizer alloc] initWithTarget:(PTMainViewController*)self.tabBarController action:@selector(nowPlaying)];
            [swipeUpDown setDirection:(UISwipeGestureRecognizerDirectionDown )];
            [self.navigationController.navigationBar addGestureRecognizer:swipeUpDown];
        }
        if (!self.navigationItem.rightBarButtonItem) {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Now Playing" style:UIBarButtonItemStyleBordered target:(PTMainViewController*)self.tabBarController action:@selector(nowPlaying)];
        }
    }
    if([self hasSearch]) {
        searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        /*the search bar widht must be > 1, the height must be at least 44
         (the real size of the search bar)*/
        
        searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
        /*contents controller is the UITableViewController, this let you to reuse
         the same TableViewController Delegate method used for the main table.*/
        
        searchDisplayController.delegate = self;
        searchDisplayController.searchResultsDataSource = self;
        //set the delegate = self. Previously declared in ViewController.h
        self.searchDisplayController.searchResultsTableView.delegate = self;
        self.tableView.tableHeaderView = searchBar; //this line add the searchBar
        //on the top of tableView.
    }
}

- (void) viewWillAppear:(BOOL)animated {
    self.navigationItem.title = [curPlaylist name];
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (tableView == self.tableView) {NSLog(@"ret sdta cnt");
        return ( [self content].count );
    }
    
    return searchdta.count;
}

- (UITableViewCellStyle) cellStyle { return UITableViewCellStyleSubtitle; }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PTTrack* trk = ( (tableView == self.tableView) ?  [self content] : searchdta )[indexPath.row];
     NSString *CellIdentifier = [trk identifier];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:[self cellStyle] reuseIdentifier:CellIdentifier];
        [cell.textLabel setText:trk.name];
        
        [cell.detailTextLabel setText:[NSString stringWithFormat:@"%@ / %@ / %@", trk.artist, trk.album, trk.totaltime]];
    }
    // Configure the cell...
    if (trk.isNowPlaying) {
        UIImageView*i = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"nowp"]];
        [i setFrame:CGRectMake(0, 0, 30, 30)];
        [cell setAccessoryView:i];
    } else {
        [cell setAccessoryView:nil];
    }
    return cell;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [searchdta removeAllObjects];
    
    for (PTTrack*t in [self content]) {
        if ([t.name containsString:searchString options:NSCaseInsensitiveSearch] ||[t.artist containsString:searchString options:NSCaseInsensitiveSearch] ||[t.album containsString:searchString options:NSCaseInsensitiveSearch] ) {
            [searchdta addObject:t];
        }
    }
    return YES;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"FIXME");
    [[PTPlayer sharedInstance] playList:curPlaylist startingFrom:(tableView == self.tableView ? indexPath.row : [[self content] indexOfObjectIdenticalTo:[searchdta objectAtIndex:indexPath.row]])];
  
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    [tableView reloadData];
    UITableViewCell*cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView*i = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"nowp"]];
    [i setFrame:CGRectMake(0, 0, 30, 30)];
    [cell setAccessoryView:i];

}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
