//
//  PTRadioStation.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PTTrack.h"
@interface PTRadioStation : PTTrack
@property NSString* streamUrl;
@property NSString* name;
@property NSString* identifier;
@property bool isNowPlaying;
@end
