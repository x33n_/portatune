//
//  PTRadiosViewController.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 06/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTPlaylistViewController.h"

@interface PTRadiosViewController : PTPlaylistViewController

@end
