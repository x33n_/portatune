//
//  PTRadiosViewController.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 06/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTRadiosViewController.h"
#import "PTUserConnection.h"
#import "PTRadioStation.h"
#import "MBProgressHUD.h"
#import "ISRefreshControl.h"
#import "PTPlayer.h"
@interface PTRadiosViewController ()
{
    PTPlaylist*radios;
}
@end

@implementation PTRadiosViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
            }
    return self;
}
- (PTPlaylist*)playlist {
    return radios;
}
- (bool) hasSearch { return false; }
- (NSArray*)content {
    return radios.contents;
}
- (UITableViewCellStyle)cellStyle {
    return UITableViewCellStyleDefault;
}
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];   self.navigationItem.title = @"Radio";
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.refreshControl = (id)[[UIRefreshControl alloc] init];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh) name:@"PortaTuneDidLogin" object:nil];
    [self.refreshControl addTarget:self
                            action:@selector(refresh)
                  forControlEvents:UIControlEventValueChanged];
}
- (void) refresh {
    [self.refreshControl beginRefreshing];
    PTUserConnection*u = [PTUserConnection sharedUserConnection];
    if ([u isLoggedIn]) {
        [[PTUserConnection sharedUserConnection]getRadiosForCallback:^(PTPlaylist *ret) {
            radios = ret ;
            [self.refreshControl endRefreshing];
            [self.tableView reloadData];
            NSLog(@"R %@",radios);
        }];
    } else {
        
    }
}

bool oneTimeRefresh;
- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!oneTimeRefresh) {
        oneTimeRefresh = true;
        [self refresh];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh) name:@"PortaTuneDidLogin" object:nil];
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

   [[PTPlayer sharedInstance] playRadio:((PTRadioStation*)[self content][indexPath.row])];
    
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    [tableView reloadData];
    UITableViewCell*cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView*i = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"nowp"]];
    [i setFrame:CGRectMake(0, 0, 30, 30)];
    [cell setAccessoryView:i];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
