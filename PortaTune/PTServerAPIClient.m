//
//  PTServerAPIClient.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTServerAPIClient.h"
#import <SystemConfiguration/CaptiveNetwork.h>

@interface PTServerAPIClient() {
    NSMutableArray *cachingOperations;
}
@end

@implementation PTServerAPIClient
+(PTServerAPIClient*) sharedClient {
	static PTServerAPIClient *sharedInstance = nil;
	if (sharedInstance == nil)
	{
		sharedInstance = [[self alloc] init];
        if (!sharedInstance->cachingOperations) {
            sharedInstance->cachingOperations = [NSMutableArray new];
        }
	}
	return sharedInstance;
}

+ (NSString *)currentWifiSSID {
    // Does not work on the simulator.
    NSString *ssid = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info[@"SSID"]) {
            ssid = info[@"SSID"];
        }
    }
    return ssid;
}

- (void) sendRequest:(NSString*)request withParameters:(NSDictionary*)params andCallback:(void(^)(id result, NSError* error))callback {
    MKNetworkOperation* oper = [[MKNetworkOperation alloc]initWithURLString:[NSString stringWithFormat:@"%@/api/%@.php",[ PTServerAPIClient getCurrentEndpoint ],request] params:params httpMethod:@"POST"];
    [oper addCompletionHandler:^(MKNetworkOperation *completedOperation) {
     // NSLog(@"RECV %@", completedOperation.responseString);
        if (callback) callback(completedOperation.responseJSON, nil);
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        if (callback)  callback(nil, completedOperation.error);
    }];
    [oper start];
}
+ (unsigned long long int)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    unsigned long long int fileSize = 0;
    
    for(NSString*fileName in filesArray) {
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:nil];
        fileSize += [fileDictionary fileSize];
    }
    return fileSize;
}

+ (NSString *) makeLinkForAudiofile: (NSString*)audioID {
    NSString* remoteUrl = [NSString stringWithFormat:@"%@/%@", [PTServerAPIClient getCurrentEndpoint], audioID];
    
    
    NSString* idInCache = [audioID stringByReplacingOccurrencesOfString:@"/" withString:@"___"];
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDir    = [myPathList  objectAtIndex:0];
    NSString* cachedpath =[[cacheDir stringByAppendingPathComponent:idInCache] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
   
    if ([self isWorkingLocally]) {
        if ([[NSUserDefaults standardUserDefaults]boolForKey:[NSString stringWithFormat:@"cache-allow-%@",[[idInCache pathExtension] lowercaseString]]]) {
            if (![[NSFileManager defaultManager]fileExistsAtPath:cachedpath]) {
                NSLog(@"Should cache %@",audioID);
                NSString*icprdir = [cacheDir stringByAppendingPathComponent:@"incomplete"];
                if (![[NSFileManager defaultManager]fileExistsAtPath:icprdir]) {
                    NSError*e;
                    [[NSFileManager defaultManager]createDirectoryAtPath:icprdir withIntermediateDirectories:true attributes:nil error:&e];
                    if(e) return remoteUrl; // give up
                }
                MKNetworkOperation*cacheOp = [[MKNetworkOperation alloc]initWithURLString:[remoteUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] params:nil httpMethod:@"GET"];
                [cacheOp addDownloadStream:[NSOutputStream
                                            outputStreamToFileAtPath:[icprdir stringByAppendingPathComponent:idInCache]
                                            append:NO]];
                [cacheOp addCompletionHandler:^(MKNetworkOperation *completedOperation) {
                    NSError*e;
                    [[NSFileManager defaultManager]moveItemAtPath:[icprdir stringByAppendingPathComponent:idInCache] toPath:cachedpath error:&e];
                    if (e) {
                        NSLog(@"Cache move error! %@",e.description);
                    }
                    [[self sharedClient]->cachingOperations removeObject:completedOperation];
                    NSLog(@"%@ was cached into %@",audioID,cachedpath);
                    [[NSUserDefaults standardUserDefaults]setObject:[NSByteCountFormatter stringFromByteCount:[self folderSize:cacheDir] countStyle:NSByteCountFormatterCountStyleFile] forKey:@"cache-size"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
                    NSLog(@"Cache error!");
                }];
                [cacheOp start];
                [[self sharedClient]->cachingOperations addObject:cacheOp];
                return remoteUrl;
            } else {
                NSLog(@"Using cached %@ -> %@",audioID,cachedpath);
                return [NSString stringWithFormat:@"file://localhost/%@", cachedpath];
            }
        } else  {
            NSLog(@"Caching of %@ not allowed",[[idInCache pathExtension] uppercaseString]);
            return remoteUrl;
        }
        
    } else{
        if ([[NSFileManager defaultManager]fileExistsAtPath:cachedpath]) {
            NSLog(@"Not local but still");
             NSLog(@"Using cached %@ -> %@",audioID,cachedpath);
            return [NSString stringWithFormat:@"file://localhost/%@", cachedpath];
        }
        NSLog(@"No local and no cache for %@ -> %@",audioID,cachedpath);
        return remoteUrl;
    }
   
}
+ (BOOL) isWorkingLocally {
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"local-enabled"]) {
        if ([[PTServerAPIClient currentWifiSSID] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"local-network"]]) {
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"local-access"])
                if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"local-access"] isEqualToString:@""])
                    return true;
        }
    }
    return false;
}
+ (NSString*)getCurrentEndpoint {
    NSString*  (^FormEp)() = ^() {
        if([PTServerAPIClient isWorkingLocally])
            return [[NSUserDefaults standardUserDefaults]objectForKey:@"local-access"];
        return [[NSUserDefaults standardUserDefaults]objectForKey:@"access-url"];
    };
    
    NSString*myEp = FormEp();
    
    if (![myEp hasPrefix:@"http://"] && ![myEp hasPrefix:@"https://"]) {
        myEp = [@"http://" stringByAppendingString:myEp];
    }
    return myEp;
}
@end
