//
//  PTSoundCloud.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 05/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PTSoundCloud : NSObject
+ (NSURL*)getTrackURLForPTTID: (NSString*)tid;
@end
