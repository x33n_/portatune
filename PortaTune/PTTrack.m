//
//  PTTrack.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTTrack.h"
#import "PTPlayer.h"
@implementation PTTrack
- (BOOL) isRadioStation { return false; }
- (NSString*) userReadableRepresentation {
    return [NSString stringWithFormat:@"%@ by %@     ",self.name,self.artist];
}
@end
