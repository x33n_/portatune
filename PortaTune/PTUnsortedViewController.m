//
//  PTPlaylistViewController.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTUnsortedViewController.h"
#import "PTTrack.h"
#import "PTPlayer.h"
#import "PTUserConnection.h"
#import "ISRefreshControl.h"
#import "PTMainViewController.h"
@interface PTUnsortedViewController ()
{
    PTPlaylist* allUnsort;
    NSArray* content;
}
@end

@implementation PTUnsortedViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.refreshControl = (id)[[UIRefreshControl alloc] init];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh) name:@"PortaTuneDidLogin" object:nil];
    [self.refreshControl addTarget:self
                            action:@selector(refresh)
                  forControlEvents:UIControlEventValueChanged];
    [self refresh];
    UISwipeGestureRecognizer *swipeUpDown = [[UISwipeGestureRecognizer alloc] initWithTarget:(PTMainViewController*)self.tabBarController action:@selector(nowPlaying)];
    [swipeUpDown setDirection:(UISwipeGestureRecognizerDirectionDown )];
    [self.navigationController.navigationBar addGestureRecognizer:swipeUpDown];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (IBAction)npbtn:(id)sender {
    [(PTMainViewController*)self.tabBarController nowPlaying];
}
- (void) refresh {
    [self.refreshControl beginRefreshing];
    PTUserConnection*u = [PTUserConnection sharedUserConnection];
    if ([u isLoggedIn]) {
        [u getUnsortedForCallback:^(PTPlaylist *playlist) {
            [self.refreshControl endRefreshing];
            allUnsort = playlist;
            content = allUnsort.contents;
            [self.tableView reloadData];
        }];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return (content ? content.count : 0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PTTrack* trk = content[indexPath.row];
     NSString *CellIdentifier = [trk identifier];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        [cell.textLabel setText:trk.name];
        
        [cell.detailTextLabel setText:[NSString stringWithFormat:@"%@ / %@ / %@", trk.artist, trk.album, trk.totaltime]];
    }
    // Configure the cell...
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"FIXME");
    [[PTPlayer sharedInstance] playList:allUnsort startingFrom:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
