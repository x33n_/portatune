//
//  PTUserConnection.h
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PTServerAPIClient.h"
#import "PTPlaylist.h"
@interface PTUserConnection : NSObject
+ (PTUserConnection*)sharedUserConnection;
- (BOOL) isLoggedIn;
- (void) loginWithUsername:(NSString*)name password:(NSString*)pass errorCallback:(void(^)(NSString* errorCode))errorc successCallback:(void(^)())success ;
- (void) getPlaylistsForCallback:(void(^)(NSArray*playlists))success;
- (void) getUnsortedForCallback:(void (^)(PTPlaylist *playlist))success ;
- (void) getAlbumsForCallback:(void (^)(NSArray *))success;
- (void) getAlbum:(PTPlaylist*)album ForCallback:(void (^)(PTPlaylist *))success;
- (void) getRadiosForCallback:(void (^)(PTPlaylist *))success;
- (void) getVkTokenWithFailure:(void(^)(NSString* errorCode))failed successful:(void(^)(NSString* token))success;
- (NSArray*)albums;
@end
