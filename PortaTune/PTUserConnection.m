//
//  PTUserConnection.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 04/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//
#import "PTTrack.h"
#import "PTRadioStation.h"
#import "PTUserConnection.h"
#import "PDKeychainBindings.h"
@interface PTUserConnection()
{
    bool didLogInSuccessfully; NSArray __block *allplaylists;
}
@end

@interface NSString (HTML)

- (NSString *)decodeHTMLCharacterEntities;
- (NSString *)encodeHTMLCharacterEntities;

@end
@implementation NSString (HTML)

// Method based on code obtained from:
// http://www.thinkmac.co.uk/blog/2005/05/removing-entities-from-html-in-cocoa.html
//

- (NSString *)decodeHTMLCharacterEntities {
    if ([self rangeOfString:@"&"].location == NSNotFound) {
        return self;
    } else {
        NSMutableString *escaped = [NSMutableString stringWithString:self];
        NSArray *codes = [NSArray arrayWithObjects:
                          @"&nbsp;", @"&iexcl;", @"&cent;", @"&pound;", @"&curren;", @"&yen;", @"&brvbar;",
                          @"&sect;", @"&uml;", @"&copy;", @"&ordf;", @"&laquo;", @"&not;", @"&shy;", @"&reg;",
                          @"&macr;", @"&deg;", @"&plusmn;", @"&sup2;", @"&sup3;", @"&acute;", @"&micro;",
                          @"&para;", @"&middot;", @"&cedil;", @"&sup1;", @"&ordm;", @"&raquo;", @"&frac14;",
                          @"&frac12;", @"&frac34;", @"&iquest;", @"&Agrave;", @"&Aacute;", @"&Acirc;",
                          @"&Atilde;", @"&Auml;", @"&Aring;", @"&AElig;", @"&Ccedil;", @"&Egrave;",
                          @"&Eacute;", @"&Ecirc;", @"&Euml;", @"&Igrave;", @"&Iacute;", @"&Icirc;", @"&Iuml;",
                          @"&ETH;", @"&Ntilde;", @"&Ograve;", @"&Oacute;", @"&Ocirc;", @"&Otilde;", @"&Ouml;",
                          @"&times;", @"&Oslash;", @"&Ugrave;", @"&Uacute;", @"&Ucirc;", @"&Uuml;", @"&Yacute;",
                          @"&THORN;", @"&szlig;", @"&agrave;", @"&aacute;", @"&acirc;", @"&atilde;", @"&auml;",
                          @"&aring;", @"&aelig;", @"&ccedil;", @"&egrave;", @"&eacute;", @"&ecirc;", @"&euml;",
                          @"&igrave;", @"&iacute;", @"&icirc;", @"&iuml;", @"&eth;", @"&ntilde;", @"&ograve;",
                          @"&oacute;", @"&ocirc;", @"&otilde;", @"&ouml;", @"&divide;", @"&oslash;", @"&ugrave;",
                          @"&uacute;", @"&ucirc;", @"&uuml;", @"&yacute;", @"&thorn;", @"&yuml;", nil];
        
        NSUInteger i, count = [codes count];
        
        // Html
        for (i = 0; i < count; i++) {
            NSRange range = [self rangeOfString:[codes objectAtIndex:i]];
            if (range.location != NSNotFound) {
                [escaped replaceOccurrencesOfString:[codes objectAtIndex:i]
                                         withString:[NSString stringWithFormat:@"%C", 160 + i]
                                            options:NSLiteralSearch
                                              range:NSMakeRange(0, [escaped length])];
            }
        }
        
        // The following five are not in the 160+ range
        
        // @"&amp;"
        NSRange range = [self rangeOfString:@"&amp;"];
        if (range.location != NSNotFound) {
            [escaped replaceOccurrencesOfString:@"&amp;"
                                     withString:[NSString stringWithFormat:@"%C", 38]
                                        options:NSLiteralSearch
                                          range:NSMakeRange(0, [escaped length])];
        }
        
        // @"&lt;"
        range = [self rangeOfString:@"&lt;"];
        if (range.location != NSNotFound) {
            [escaped replaceOccurrencesOfString:@"&lt;"
                                     withString:[NSString stringWithFormat:@"%C", 60]
                                        options:NSLiteralSearch
                                          range:NSMakeRange(0, [escaped length])];
        }
        
        // @"&gt;"
        range = [self rangeOfString:@"&gt;"];
        if (range.location != NSNotFound) {
            [escaped replaceOccurrencesOfString:@"&gt;"
                                     withString:[NSString stringWithFormat:@"%C", 62]
                                        options:NSLiteralSearch
                                          range:NSMakeRange(0, [escaped length])];
        }
        
        // @"&apos;"
        range = [self rangeOfString:@"&apos;"];
        if (range.location != NSNotFound) {
            [escaped replaceOccurrencesOfString:@"&apos;"
                                     withString:[NSString stringWithFormat:@"%C", 39]
                                        options:NSLiteralSearch
                                          range:NSMakeRange(0, [escaped length])];
        }
        
        // @"&quot;"
        range = [self rangeOfString:@"&quot;"];
        if (range.location != NSNotFound) {
            [escaped replaceOccurrencesOfString:@"&quot;"
                                     withString:[NSString stringWithFormat:@"%C", 34]
                                        options:NSLiteralSearch
                                          range:NSMakeRange(0, [escaped length])];
        }
        
        // Decimal & Hex
        NSRange start, finish, searchRange = NSMakeRange(0, [escaped length]);
        i = 0;
        
        while (i < [escaped length]) {
            start = [escaped rangeOfString:@"&#"
                                   options:NSCaseInsensitiveSearch
                                     range:searchRange];
            
            finish = [escaped rangeOfString:@";"
                                    options:NSCaseInsensitiveSearch
                                      range:searchRange];
            
            if (start.location != NSNotFound && finish.location != NSNotFound &&
                finish.location > start.location) {
                NSRange entityRange = NSMakeRange(start.location, (finish.location - start.location) + 1);
                NSString *entity = [escaped substringWithRange:entityRange];
                NSString *value = [entity substringWithRange:NSMakeRange(2, [entity length] - 2)];
                
                [escaped deleteCharactersInRange:entityRange];
                
                if ([value hasPrefix:@"x"]) {
                    unsigned tempInt = 0;
                    NSScanner *scanner = [NSScanner scannerWithString:[value substringFromIndex:1]];
                    [scanner scanHexInt:&tempInt];
                    [escaped insertString:[NSString stringWithFormat:@"%C", tempInt] atIndex:entityRange.location];
                } else {
                    [escaped insertString:[NSString stringWithFormat:@"%C", [value intValue]] atIndex:entityRange.location];
                } i = start.location;
            } else { i++; }
            searchRange = NSMakeRange(i, [escaped length] - i);
        }
        
        return escaped;    // Note this is autoreleased
    }
}

- (NSString *)encodeHTMLCharacterEntities {
    NSMutableString *encoded = [NSMutableString stringWithString:self];
    
    // @"&amp;"
    NSRange range = [self rangeOfString:@"&"];
    if (range.location != NSNotFound) {
        [encoded replaceOccurrencesOfString:@"&"
                                 withString:@"&amp;"
                                    options:NSLiteralSearch
                                      range:NSMakeRange(0, [encoded length])];
    }
    
    // @"&lt;"
    range = [self rangeOfString:@"<"];
    if (range.location != NSNotFound) {
        [encoded replaceOccurrencesOfString:@"<"
                                 withString:@"&lt;"
                                    options:NSLiteralSearch
                                      range:NSMakeRange(0, [encoded length])];
    }
    
    // @"&gt;"
    range = [self rangeOfString:@">"];
    if (range.location != NSNotFound) {
        [encoded replaceOccurrencesOfString:@">"
                                 withString:@"&gt;"
                                    options:NSLiteralSearch
                                      range:NSMakeRange(0, [encoded length])];
    }
    
    return encoded;
}

@end

@implementation PTUserConnection
+ (PTUserConnection*)sharedUserConnection {
	static PTUserConnection *sharedInstance = nil;
	if (sharedInstance == nil)
	{
		sharedInstance = [[self alloc] init];
	}
	return sharedInstance;
}
- (void) _sessionKeepAlive{
    [[PTServerAPIClient sharedClient]sendRequest:@"keepalive" withParameters:Nil andCallback:^(id result, NSError *error) {
        if (!error) {
            [self performSelector:@selector(_sessionKeepAlive) withObject:nil afterDelay:60];
        }
    }];
}
- (BOOL) isLoggedIn {
    return didLogInSuccessfully;
}
- (void) loginWithUsername:(NSString*)name password:(NSString*)pass errorCallback:(void(^)(NSString* errorCode))errorc successCallback:(void(^)())success {
    [[PTServerAPIClient sharedClient]sendRequest:@"signin" withParameters:@{@"login": name, @"pass":pass} andCallback:^(id result, NSError *error) {
        if (error) {
            if (errorc) {
                errorc(@"Network error");
            }
            return;
        }
        if (result) {
            NSDictionary* response = result;
            if ([response[@"code"]isEqualToString:@"failure"]) {
                if (errorc) {
                    errorc(@"Invalid credentials");
                }
                return;
            }
            else if ([response[@"code"]isEqualToString:@"success"]) {
                didLogInSuccessfully = true;
                if(success)success();
                [self _sessionKeepAlive];
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"PortaTuneDidLogin" object:nil];
            } else {
                if (errorc) {
                    errorc(@"Unknown server response");
                }
                return;
            }
            
        } else {
            if (errorc) {
                errorc(@"No server response");
            }
        }
    }];

}

- (void) getPlaylistsForCallback:(void (^)(NSArray *))success {
    if ([self isLoggedIn]) {
        [[PTServerAPIClient sharedClient]sendRequest:@"playlists" withParameters:nil andCallback:^(id result, NSError *error) {
            if (error) {
                [[[UIAlertView alloc]initWithTitle:@"Error" message:[@"The following error occured while getting playlists: " stringByAppendingString:error.localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
            }
            else {
                if (result[@"result"]) {
                    NSMutableArray *pp = [NSMutableArray new];
                    for (NSDictionary*plsRep in result[@"result"]) {
                        PTPlaylist* pls = [PTPlaylist new];
                        [pls setName:plsRep[@"name"]];
                        NSJSONSerialization*ses = [NSJSONSerialization JSONObjectWithData:[plsRep[@"contents"] dataUsingEncoding:NSUTF8StringEncoding] options:nil error:nil];
                        NSMutableArray*pc = [NSMutableArray new];
                        for (NSDictionary*sas in ses) {
                            PTTrack* trk = [PTTrack new];
                            [trk setName:[([sas[@"title"] isKindOfClass:[NSNull class]] ?@"Unknown Track": sas[@"title"] ) decodeHTMLCharacterEntities]];
                            [trk setAlbum:[([sas[@"album"] isKindOfClass:[NSNull class]]? @"Some Album":sas[@"album"]) decodeHTMLCharacterEntities]];
                            [trk setArtist:[([sas[@"artist"] isKindOfClass:[NSNull class]]?@"Some Artist": sas[@"artist"] ) decodeHTMLCharacterEntities]];
                            [trk setTotaltime:[([sas[@"time"] isKindOfClass:[NSNull class]]? @"--:--":sas[@"time"] ) decodeHTMLCharacterEntities]];
                            [trk setIdentifier:sas[@"fname"]];
                            [trk setContainingPlaylist:pls];
                            [pc addObject:trk];
                        }
                        [pls setContents:pc];
                        [pls setIdentifier:[plsRep[@"id"] integerValue]];
                        [pp addObject:pls];
                    }
                    if(success) success(pp);
                }
            }
        }];
    } else {
        [self loginWithUsername:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"login"] password:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"password"] errorCallback:^(NSString *errorCode) {
            [[[UIAlertView alloc]initWithTitle:@"Error" message:[@"The following error occured while getting playlists: " stringByAppendingString:errorCode] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        } successCallback:^{
            [self getPlaylistsForCallback:success];
        }];
    }
    
}

- (void) getAlbumsForCallback:(void (^)(NSArray *))success {
    if ([self isLoggedIn]) {
        [[PTServerAPIClient sharedClient]sendRequest:@"albums" withParameters:nil andCallback:^(id result, NSError *error) {
            if (error) {
                [[[UIAlertView alloc]initWithTitle:@"Error" message:[@"The following error occured while getting albums: " stringByAppendingString:error.localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
            }
            else {
                if (result[@"result"]) {
                    NSMutableArray *pp = [NSMutableArray new];
                    for (NSDictionary*plsRep in result[@"result"]) {
                        PTPlaylist* pls = [PTPlaylist new];
                        [pls setName:plsRep[@"name"]];
                        [pls setIdentifier:[plsRep[@"id"] integerValue]];
                        [pls setArtist:plsRep[@"artist"]];
                        [pls setIsAlbum:true];
                        [pp addObject:pls];
                    }
                    if(success) success(pp);
                }
            }
        }];
    } else {
        [self loginWithUsername:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"login"] password:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"password"] errorCallback:^(NSString *errorCode) {
            [[[UIAlertView alloc]initWithTitle:@"Error" message:[@"The following error occured while getting albums: " stringByAppendingString:errorCode] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        } successCallback:^{
            [self getAlbumsForCallback:success];
        }];
    }
    
}

- (void) getVkTokenWithFailure:(void(^)(NSString* errorCode))failed  successful:(void(^)(NSString* token))success {
    if ([[PDKeychainBindings sharedKeychainBindings]objectForKey:@"vktoken"] && ![[[PDKeychainBindings sharedKeychainBindings]objectForKey:@"vktoken"] isEqualToString:@""]) {
        if(success) success([[PDKeychainBindings sharedKeychainBindings]objectForKey:@"vktoken"]); return;
    }
    if ([self isLoggedIn]) {
        [[PTServerAPIClient sharedClient]sendRequest:@"vktoken" withParameters:nil andCallback:^(id result, NSError *error) {
            if (error) {
                [[[UIAlertView alloc]initWithTitle:@"Error" message:[@"The following error occured while accesing VK token: " stringByAppendingString:error.localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
            }
            else {
                if (result[@"result"]) {
                    [[PDKeychainBindings sharedKeychainBindings]setObject:result[@"result"] forKey:@"vktoken"];
                    if(success) success([[PDKeychainBindings sharedKeychainBindings]objectForKey:@"vktoken"]);
                } else if (result[@"detail"])
                    if ([result[@"detail"] isEqualToString:@"noneAvailable"])
                        if(failed) failed(@"No access token available. Please login to VK from your PC via WebTunes.");
            }
        }];
    } else {
        [self loginWithUsername:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"login"] password:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"password"] errorCallback:^(NSString *errorCode) {
            [[[UIAlertView alloc]initWithTitle:@"Error" message:[@"The following error occured while accesing VK token: " stringByAppendingString:errorCode] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        } successCallback:^{
            [self getVkTokenWithFailure:failed successful:success];
        }];
    }
}

- (void) getRadiosForCallback:(void (^)(PTPlaylist *))success {
    if ([self isLoggedIn]) {
        [[PTServerAPIClient sharedClient]sendRequest:@"radio" withParameters:nil andCallback:^(id result, NSError *error) {
            if (error) {
                [[[UIAlertView alloc]initWithTitle:@"Error" message:[@"The following error occured while getting radios: " stringByAppendingString:error.localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
            }
            else {
                if (result[@"result"]) {
                    NSMutableArray *pp = [NSMutableArray new];
                    PTPlaylist* pls = [PTPlaylist new];
                    for (NSDictionary*sas in result[@"result"]) {
                        PTRadioStation* s = [PTRadioStation new];
                        [s setName:[([sas[@"name"] isKindOfClass:[NSNull class]] ?@"Unknown Station": sas[@"name"] ) decodeHTMLCharacterEntities]];
                        [s setStreamUrl:sas[@"url"]];
                        [s setIdentifier:sas[@"url"]];
                        [pp addObject:s];
                    }
                    [pls setContents:pp];
                    [pls setName:@"Radio"];
                    if(success) success(pls);
                }
            }
        }];
    } else {
        [self loginWithUsername:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"login"] password:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"password"] errorCallback:^(NSString *errorCode) {
            [[[UIAlertView alloc]initWithTitle:@"Error" message:[@"The following error occured while getting radios: " stringByAppendingString:errorCode] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        } successCallback:^{
            [self getRadiosForCallback:success];
        }];
    }
    
}


- (void) getAlbum:(PTPlaylist*)album ForCallback:(void (^)(PTPlaylist *))success {
    if ([self isLoggedIn]) {
        [[PTServerAPIClient sharedClient]sendRequest:@"album" withParameters:@{@"album": [NSNumber numberWithInteger:album.identifier]} andCallback:^(id result, NSError *error) {
            if (error) {
                [[[UIAlertView alloc]initWithTitle:@"Error" message:[@"The following error occured while getting album: " stringByAppendingString:error.localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
            }
            else {
                if (result[@"result"]) {
                        PTPlaylist* pls = album;
                        NSMutableArray*pc = [NSMutableArray new];
                        for (NSDictionary*sas in result[@"result"]) {
                            PTTrack* trk = [PTTrack new];
                            [trk setName:[([sas[@"title"] isKindOfClass:[NSNull class]] ?@"Unknown Track": sas[@"title"] ) decodeHTMLCharacterEntities]];
                            [trk setAlbum:[([album.name isKindOfClass:[NSNull class]]? @"Some Album":album.name) decodeHTMLCharacterEntities]];
                            [trk setArtist:[([album.artist isKindOfClass:[NSNull class]]?@"Some Artist": album.artist ) decodeHTMLCharacterEntities]];
                            [trk setTotaltime:[([sas[@"time"] isKindOfClass:[NSNull class]]? @"--:--":sas[@"time"] ) decodeHTMLCharacterEntities]];
                            [trk setIdentifier:sas[@"fname"]];
                            [trk setContainingPlaylist:pls];
                            [pc addObject:trk];
                        }
                        [pls setContents:pc];
                    if(success) success(pls);
                }
            }
        }];
    } else {
        [self loginWithUsername:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"login"] password:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"password"] errorCallback:^(NSString *errorCode) {
            [[[UIAlertView alloc]initWithTitle:@"Error" message:[@"The following error occured while getting album: " stringByAppendingString:errorCode] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        } successCallback:^{
            [self getAlbum:album ForCallback:success];
        }];
    }
    
}



- (void) getUnsortedForCallback:(void (^)(PTPlaylist *playlist))success {
    if ([self isLoggedIn]) {
        [[PTServerAPIClient sharedClient]sendRequest:@"unsorted" withParameters:nil andCallback:^(id result, NSError *error) {
            if (error) {
                [[[UIAlertView alloc]initWithTitle:@"Error" message:[@"The following error occured while getting playlists: " stringByAppendingString:error.localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
            }
            else {
                if (result[@"result"]) {
                    NSMutableArray *pp = [NSMutableArray new]; PTPlaylist* pls = [PTPlaylist new];
                    for (NSDictionary*sas in result[@"result"]) {
                        NSLog(@"Got track %@",sas);
                        PTTrack* trk = [PTTrack new];
                            [trk setName:[([sas[@"title"] isKindOfClass:[NSNull class]] ?@"Unknown Track": sas[@"title"] ) decodeHTMLCharacterEntities]];
                            [trk setAlbum:[([sas[@"album"] isKindOfClass:[NSNull class]]? @"Some Album":sas[@"album"]) decodeHTMLCharacterEntities]];
                            [trk setArtist:[([sas[@"artist"] isKindOfClass:[NSNull class]]?@"Some Artist": sas[@"artist"] ) decodeHTMLCharacterEntities]];
                            [trk setTotaltime:[([sas[@"time"] isKindOfClass:[NSNull class]]? @"--:--":sas[@"time"] ) decodeHTMLCharacterEntities]];
                            [trk setIdentifier:sas[@"fname"]];
                            [trk setContainingPlaylist:pls];
                            [pp addObject:trk];
                        }
                   
                        [pls setContents:pp];
                        [pls setIdentifier:-1];
                    [pls setName:@"Unsorted"];
                    if(success) success(pls);
                    }
                
                }
        }];
    } else {
        [self loginWithUsername:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"login"] password:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"password"] errorCallback:^(NSString *errorCode) {
            [[[UIAlertView alloc]initWithTitle:@"Error" message:[@"The following error occured while getting unsorted folder: " stringByAppendingString:errorCode] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        } successCallback:^{
            [self getUnsortedForCallback:success];
        }];
    }
    
}
- (NSArray*)unsorted {
    return  nil;
}
- (NSArray*)albums {
    return nil;
}
@end
