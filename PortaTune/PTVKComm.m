//
//  PTVKComm.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 06/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTVKComm.h"
#import "MKNetworkKit.h"
#import "PTUserConnection.h"
#import "ALAlertBanner.h"
@implementation PTVKComm
+ (void) resolveTrack:(PTTrack*)track callback:(void (^)(PTTrack *))success {
    [[PTUserConnection sharedUserConnection]getVkTokenWithFailure:^(NSString *errorCode) {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:[@"Error accessing VK token: "stringByAppendingString:errorCode] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil]show];
    } successful:^(NSString *token) {
        
        MKNetworkOperation* recvOp = [[MKNetworkOperation alloc]initWithURLString:@"https://api.vk.com/method/audio.getById" params:@{@"access_token": token, @"audios":[track.identifier substringFromIndex:2]} httpMethod:@"GET"];
        [recvOp addCompletionHandler:^(MKNetworkOperation *completedOperation) {
            if (completedOperation.responseJSON[@"response"][0][@"url"]) {
                track.extradata = completedOperation.responseJSON[@"response"][0][@"url"];
                if(success) success(track);
            }
        } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
            [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Error when connecting to VK" delegate:self cancelButtonTitle:@"Oh well" otherButtonTitles: nil]show];
        }];
        [recvOp start];

    }];
   }
@end
