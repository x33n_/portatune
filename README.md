# PortaTune
#### a portable native WebTunes player for iOS

 Please note this is alpha software and might not work just as expected. Written in just about two lunch breaks, this may easily contain bugs or some other unexpected features

----

### Features
* Play all the tracks you have on your WebTunes
* Access your WebTunes playlists (not SoundCloud/VK sets just yet)
* Control using your headset or iPhone media controls
* Supports iOS 5.0.1 and later
* Sleek UIKit-only minimalistic design with a cool animation to reveal the controls (pull top bar down or tap Now Playing) 

	![screencast](http://cs538223.vk.me/u174350119/docs/a644dbd2e635/PTnowplaying.gif)

* Current queue is reorderable — just tap and hold on a track, then move it up or down.
* Local access mode, for when you are on your home network, and your WebTunes server is, too, it will use local URL instead of outer network address

	![screenie](http://cs312728.vk.me/v312728119/495a/-Cqr4hRGJP8.jpg)

----

### Requirements
* A working [WebTunes](https://bitbucket.org/vladkorotnev/webtunes) server reachable on the internet and/or the local network
* An account on the WebTunes server
* An iPhone with iOS 5.0.1 or later (iOS 6 and iOS 7 were not tested).
* A stable internet connection (3G or WiFi)
* An unlimited data plan is recommended

----

### Thanks to
* The VideoLAN team for VLCKit
* Mugunth Kumar for MKNetworkEngine
* Carl Brown for PDKeychainBindings
* Matej Bukovinski for MBProgressHUD
* Yosuke Ishikawa for ISRefreshControl
* Anthony Lobianco for ALAlertBanner
* Jonathan Willing for JWFolders
* Charles Powell for MarqueeLabel